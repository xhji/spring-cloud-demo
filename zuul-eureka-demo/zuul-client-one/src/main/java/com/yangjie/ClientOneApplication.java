package com.yangjie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 客户端服务
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ClientOneApplication {
    public static void main(String[] args) {
        SpringApplication.run(ClientOneApplication.class, args);
    }

}
