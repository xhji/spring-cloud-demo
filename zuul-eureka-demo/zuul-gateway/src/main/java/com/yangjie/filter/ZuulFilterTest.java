package com.yangjie.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

/**
 * @author yangjie
 * @date 2020/4/14
 */
@Component
public class ZuulFilterTest extends ZuulFilter {

    //返回为true才会执行
    public boolean shouldFilter() {
        return true;
    }

    public Object run() throws ZuulException {
        System.out.println("执行了里面的逻辑啦");
        return "true";
    }

    //ZuulFilter的类型
    @Override
    public String filterType() {
        return FilterConstants.ROUTE_TYPE;
    }

    /**
     * 优先级
     */
    @Override
    public int filterOrder() {
        return 0;
    }


}
