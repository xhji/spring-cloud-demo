package com.yangjie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yangjie
 * @date 2020/3/31
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private ResumeClient resumeClient;

    @GetMapping("/login")
    private void login(){
        String login = resumeClient.login(LoginVo.builder().account("admin").password("123").build());
        System.out.println(login);
    }
}
