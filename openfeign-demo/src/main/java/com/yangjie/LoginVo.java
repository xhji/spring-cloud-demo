package com.yangjie;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yangjie
 * @date 2020/3/31
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginVo {
    private String account;
    private String password;
}
