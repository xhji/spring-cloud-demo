package com.yangjie;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author yangjie
 * @date 2020/3/31
 */
@FeignClient(name = "local", url = "${test-url}")
@RequestMapping(value = "/user", produces = {"application/json;charset=UTF-8"})
public interface ResumeClient {
    @PostMapping(value = "/login")
    String login(@RequestBody LoginVo loginVo);
}